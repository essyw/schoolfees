package controllers;

import play.mvc.*;

import views.html.*;
import play.db.*;
import javax.inject.*;
import play.data.*;
import services.SchoolOperations;
import services.Users;
import services.Login;
import services.Students;
import services.Roles;
import services.Clases;
import services.Streams;
import services.Fees;
import services.Feesdetail;

import forms.Studentdetails;
import services.AdminOnlySecurity;
import java.util.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */

public class AdminController extends Controller {
   private SchoolOperations schoolop;
    play.data.FormFactory fmm;
   @ Inject

public AdminController(SchoolOperations schoolop, play.data.FormFactory fmm){
  this.schoolop=schoolop;
  this.fmm=fmm;
}
    public Result index() {
        return ok(index.render("Your new application is ready."));
    }
    public Result homepage() {
        return ok(homepage.render());
    }
    public Result registeruser() {
        Users us = new Users();
        ArrayList<Roles> role=  schoolop.getRole(us);
        int no = role.size();
       System.out.println("the no of roles are....>"+no);
    if(request().method().equals("GET")){
        return ok(registeruser.render(role));   
    }else{
        DynamicForm requestData =fmm.form().bindFromRequest();
        Users newUser = new Users(requestData.get("name"),requestData.get("email"),
        requestData.get("password"),requestData.get("role"));
       boolean reg=schoolop.registerUser(newUser);
       if(reg==true){
        flash("message","User Registered");
            return  redirect("/registeruser");
       }else{
       flash("message","user Registration failed");
       }
    }
        return ok(registeruser.render(role));
    }
    public Result addrole(){
        Users us = new Users();
        ArrayList<Roles> role=  schoolop.getRole(us);
        if(request().method().equals("GET")){
            return ok(addrole.render(role));
        }else{
        Form<Roles> roleform= fmm.form(Roles.class);
        Roles newrole= roleform.bindFromRequest().get();
       boolean ans= schoolop.addRole(newrole);
        if(ans==true){
            flash("message","role added");
                return  redirect("/addrole");
           }else{
           flash("message","role addition failed");
           }
        }
        return ok(addrole.render(role)); 
        
    }
    public Result addclass(){
        ArrayList<Clases> clas= schoolop.allclases();
        if(request().method().equals("GET")){
            return ok(addclass.render(clas));
        }else{
            DynamicForm requestData =fmm.form().bindFromRequest();
           String scul= requestData.get("school_id");
           int scl=0;
           try{
            scl=Integer.parseInt(scul);
        }catch(NumberFormatException e){
        }
            Clases newclas = new Clases(requestData.get("class_name"),scl);
        //Form<Clases> clasform=fmm.form(Clases.class);
        //Clases newclas=clasform.bindFromRequest().get();
        boolean ans=schoolop.addclass(newclas);
        if(ans==true){
            flash("message","class added");
                return  redirect("/addclass");
           }else{
           flash("message","class addition failed");
           }
        }
        return ok(addclass.render(clas));
    }
    public Result allclass(){
        ArrayList<Clases> clas= schoolop.allclases();
  return ok(allclass.render(clas));
    }
  public Result fee(String clas){
    int claz = Integer.parseInt(clas);
    ArrayList<Feesdetail> fees= schoolop.allFees(claz);
    Feesdetail detail = new Feesdetail(claz);
    Fees fe=new Fees(claz);  
    if(request().method().equals("GET")){ 
      return ok(fee.render(fees,detail,fe)); 
    }else{
        Form<Fees> feeform= fmm.form(Fees.class);
        Fees sfee = feeform.bindFromRequest().get();
        boolean ans=schoolop.addfee(sfee,claz);
   
    }
    return ok(fee.render(fees,detail,fe));
}
    @Security.Authenticated(AdminOnlySecurity.class)
    public Result editclass(String class_id,String class_name) {
        int id=Integer.parseInt(class_id);
        Clases clas = new Clases(class_name,id);
        ArrayList<Clases> cls= schoolop.allclases();
        if(request().method().equals("GET")){
            return ok(editclass.render(cls,clas));
        }else{
            Form<Clases> classform= fmm.form(Clases.class);
            clas= classform.bindFromRequest().get();
           schoolop.editclas(clas);
        }
        return redirect("/addclass");
    }
    @Security.Authenticated(AdminOnlySecurity.class)
    public Result deleteclass(String class_id,String class_name) {
        int id=Integer.parseInt(class_id);
        Clases clas = new Clases(class_name,id);
        ArrayList<Clases> cls= schoolop.allclases();
        if(request().method().equals("GET")){
            return ok(deleteclass.render(cls,clas));
        }else{
            Form<Clases> classform= fmm.form(Clases.class);
            clas= classform.bindFromRequest().get();
           schoolop.deleteClas(clas);
        }
        return redirect("/addclass");
    }
    public Result addstream(){
        ArrayList<Streams> stream =schoolop.allStreams();
        if(request().method().equals("GET")){
            return ok(addstream.render(stream ));
        }else{
        Form<Streams> streamform=fmm.form(Streams.class);
        Streams newstream=streamform.bindFromRequest().get();
        boolean ans=schoolop.addstream(newstream);
        if(ans==true){
            flash("message","stream added");
                return  redirect("/addstream");
           }else{
           flash("message","stream addition failed");
           }
        }
        return ok(addstream.render(stream ));
    }
    public Result allstreams(){
        Streams strm =  new Streams();
        Streams streamz=schoolop.searchStream(strm);
        ArrayList<Streams> stream =schoolop.allStreams();
        if(request().method().equals("GET")){
            return ok(allstreams.render(stream,streamz ));
        }else{
        Form<Streams> streamform=fmm.form(Streams.class);
        Streams newstream=streamform.bindFromRequest().get();
        boolean ans=schoolop.addstream(newstream);
   
        return ok(allstreams.render(stream,streamz ));
    }
}
    @Security.Authenticated(AdminOnlySecurity.class)
    public Result editstream(String stream_id,String stream_name,String class_id) {
        int id=Integer.parseInt(class_id);
        Streams strm = new Streams(stream_id,stream_name,id);
        ArrayList<Streams> stream= schoolop.allStreams();
        if(request().method().equals("GET")){
            return ok(editstream.render(strm));
        }else{
            Form<Streams> streamform= fmm.form(Streams.class);
            strm= streamform.bindFromRequest().get();
           schoolop.edistream(strm);
        }
        return redirect("/addstream");
    }
    @Security.Authenticated(AdminOnlySecurity.class)
    public Result deletestream(String stream_id,String stream_name,String class_id) {
        int id=Integer.parseInt(class_id);
        Streams strm = new Streams(stream_id,stream_name,id);
        ArrayList<Streams> stream= schoolop.allStreams();
        if(request().method().equals("GET")){
            return ok(deletestream.render(strm));
        }else{
            Form<Streams> streamform= fmm.form(Streams.class);
            strm= streamform.bindFromRequest().get();
           schoolop.deleteStrem(strm);
        }
        return redirect("/addstream");
    
    }
    public Result registerstudent() {
        ArrayList<Streams> stream=schoolop.allStreams();
        ArrayList<Students> stud=schoolop.allStudent();
        if(request().method().equals("GET")){
            return ok(registerstudent.render(stud,stream));   
        }else{;
            DynamicForm requestData =fmm.form().bindFromRequest();
         String contact=requestData.get("contacts");
         int cont=0;
         try{
         cont=Integer.parseInt(contact);
        }catch(NumberFormatException e){
        }   
           Students newstudent = new Students(requestData.get("name"),requestData.get("admno"),
         requestData.get("grade"),requestData.get("dob"),requestData.get("guardian"),cont);
            //Form<Students> studentForm = fmm.form(Students.class);
            //Students newstudent = studentForm.bindFromRequest().get();
            schoolop.registerStudent(newstudent);
        return ok(registerstudent.render(stud,stream));
    }
}
public Result allstudents() {
    ArrayList<Students> stud=schoolop.allStudent();
    
        return ok(allstudents.render(stud)); 
    }
@Security.Authenticated(AdminOnlySecurity.class)
public Result editstudent(String admno,String name,String grade) {
    Students std = new Students(admno,name,grade);
    ArrayList<Streams> stream=schoolop.allStreams();
    ArrayList<Students> stud=schoolop.allStudent();
    if(request().method().equals("GET")){
        return ok(editstudent.render(stud,stream,std));
    }else{
        DynamicForm requestData =fmm.form().bindFromRequest();
            Students newstudent = new Students(requestData.get("admno"),requestData.get("name"),
            requestData.get("grade"));
        //Form<Students> studentForm = fmm.form(Students.class);
        //Students newstudent = studentForm.bindFromRequest().get();
      schoolop.editstud(newstudent);
    }
    return redirect("/registerstudent");
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result deletestudent(String admno,String name,String grade) {
    Students std = new Students(admno,name,grade);
    ArrayList<Students> stud=schoolop.allStudent();
    ArrayList<Streams> stream=schoolop.allStreams();
    if(request().method().equals("GET")){
        return ok(deletestudent.render(stud,stream,std));
    }else{
        Form<Students> studentform= fmm.form(Students.class);
        std= studentform.bindFromRequest().get();
       schoolop.deleteStud(std);
    }
    return redirect("/registerstudent");

}
public  Result managestudent() {
    ArrayList<Students> stud=schoolop.allStudent();
        return ok(managestudent.render(stud)); 
    }


}
