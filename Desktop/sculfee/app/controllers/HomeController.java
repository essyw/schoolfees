package controllers;

import play.mvc.*;

import views.html.*;
import play.db.*;
import javax.inject.*;
import play.data.*;
import java.util.*; 
import services.Portal;
import forms.Schools;
import forms.Allschools;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {
private Portal portal;
play.data.FormFactory formm;   
@Inject
public HomeController( Portal portal,play.data.FormFactory formm){
    this.portal=portal;
    this.formm=formm;
}
    public Result index() {
        return ok(index.render("Your new application is ready."));
    }
    public Result adminregistration() {
        return ok(index.render("Your new application is ready."));
    }
    public Result createschool() {
        Schools newscul = new Schools();
        ArrayList<Schools> school =portal.allSchools();
        if(request().method().equals("GET")){
            return ok(createschool.render(school,newscul));
        }else{
            Form<Schools> schoolform= formm.form(Schools.class);
             newscul= schoolform.bindFromRequest().get();
           boolean ans= portal.createSchool(newscul);
            if(ans==true){
                flash("message","school added");
                flash("title","Message Creation");
                    return  redirect("/createschool");
               }else{
               flash("message","school addition failed");
               }   
        }
        return ok(createschool.render(school,newscul));
    }
    public Result allschools() {
        Allschools als = new Allschools();
        Allschools allscul = portal.allSchoolcount(als);
        Schools newscul = new Schools();
        ArrayList<Allschools> scul= portal.searchSchools(als);
        ArrayList<Schools> school =portal.allSchools();
        System.out.println(school.get(0).getSchool_name());
        return ok(allschools.render(school,allscul,scul));
    }
    public Result editschool(String school_id,String school_name) {
        int sculid =Integer.parseInt(school_id);
        Schools scul = new Schools(sculid,school_name);
        if(request().method().equals("GET")){
            return ok(editschool.render(scul));
        }else{
            Form<Schools> schoolform= formm.form(Schools.class);
            scul= schoolform.bindFromRequest().get();
           portal.editSchool(scul);
        }
        return redirect("/createschool");
    }
   
    public Result deleteschool(String school_id,String school_name) {
        int sculid =Integer.parseInt(school_id);
        Schools scul = new Schools(sculid,school_name);
        ArrayList<Schools> school =portal.allSchools();
        if(request().method().equals("GET")){
            return ok(deleteschool.render(school,scul));
        }else{
            Form<Schools> schoolform= formm.form(Schools.class);
            Schools newscul= schoolform.bindFromRequest().get();
           portal.deleteSchool(scul);
        }
        return redirect("/createschool");
    }
    public Result scholhomepage() {
        return ok(scholhomepage .render());
    }
    public Result schoolview(String school_id,String school_name) {
       int sculid =Integer.parseInt(school_id);
        Schools scul = new Schools(sculid,school_name);
        session("schoolname",scul.getSchool_name());
        int scull=scul.getSchool_id();
        session("schoolid",scull+"");
        return ok(schoolview.render(scul));
    }
}
