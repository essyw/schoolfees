/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

/**
 *
 * @author essy
 */
public class Allschools {
    private String school_id;
    private String school_name;
    private int allschool;

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }
    public int getAllschool() {
        return allschool;
    }
    public void setAllschool(int allschool) {
        this.allschool = allschool;
    }

    public  Allschools(String school_id, String school_name,int allschool) {
        this.school_id = school_id;
        this.school_name = school_name;
        this.allschool = allschool;
    }
    public  Allschools(){
        
    } 
    public  Allschools(String school_name){
        this.school_name = school_name; 
    } 
    public  Allschools(String school_id, String school_name) {
        this.school_id = school_id;
        this.school_name = school_name;
    }
}
