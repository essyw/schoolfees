/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import javax.inject.Singleton;
import javax.inject.*;
import play.db.*;
import java.util.*; 
import java.text.SimpleDateFormat;

import forms.Studentdetails;
/**
 *
 * @author essy
 */
@Singleton
public class SchoolOperations {
 Database db;
 @Inject
 public SchoolOperations(Database db){
     this.db=db; 
 }
public boolean addRole(Roles newrole){
    Connection con =this.db.getConnection();
boolean ans=false;
    try{
        Statement st = con.createStatement();
        String add="insert into roles (role_id,role_name) values('"+newrole.getRole_id()+"','"+newrole.getRole_name()+"')";
        st.execute(add);
        ans=true;
    st.close();
    con.close();
    }catch (Exception e){
        e.printStackTrace();
    }
    return ans;
}
 public ArrayList getRole(Users newuser){
    boolean registerd=false;
    ArrayList<Roles> role = new ArrayList<Roles>();
    Connection con =this.db.getConnection();
    try{
       Statement st = con.createStatement();
       String select = "Select role_name from roles";
       ResultSet rs = st.executeQuery(select);
       while(rs.next()){
           String rolename=rs.getString("role_name");
           Roles rl = new Roles(rolename);
           role.add(rl);
       }
        st.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return role;
}
 public boolean registerUser(Users newuser){
     boolean registerd=false;
     ArrayList<Roles> role = new ArrayList<Roles>();
     Connection con =this.db.getConnection();
     try{
        Statement st = con.createStatement();
         String insert="insert into users(name,username,password,role)values('"+newuser.getName()+
                 "','"+newuser.getUserName()+"','"+newuser.getPassword()+"','"+newuser.getRole()+"')" ;
         st.execute(insert);
         System.out.println(insert);
         registerd=true;
         st.close();
         con.close();
     }catch(Exception e){
         e.printStackTrace();
     }
     return registerd;
 }
 public Login getLogin(Login newlogin){
    Connection con =this.db.getConnection();
    Login log=null;
    String pattern = "yyyy-MM-dd HH:mm:ss";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    try{
        String select="select name,username,password,role from users where username ='"
        +newlogin.getEmail()+"' && password='"+newlogin.getPassword()+"'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(select);
        while(rs.next()){
            String named=rs.getString("name");
            newlogin.setName(named);
            String emaild=rs.getString("username");
            String password=rs.getString("password");
            String roled=rs.getString("role");
            newlogin.setRole(roled);
            log = new Login(named,emaild,password,roled);
        }
        String time="update users set logintime='"+ simpleDateFormat.format(new Date())+"' where username='"+newlogin.getEmail()+"' && password='"+newlogin.getPassword()+"'";
        Statement stt = con.createStatement();
        stt.execute(time);
        st.close();
         con.close();
    }catch(Exception e){
      e.printStackTrace();  
    }
    return log; 
 }
 public boolean addclass(Clases newclas){
    Connection con =this.db.getConnection();
    boolean ans=false;
    try{
        String insert="insert into clases(class_name)values('"+newclas.getClass_name()+"')" ;
        Statement st = con.createStatement();
        st.execute(insert);
        ans=true;
        st.close();
         con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return ans;
}
public ArrayList allclases(){
    ArrayList<Clases> clas = new ArrayList<Clases>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select class_id,class_name from clases";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             int id =rs.getInt("class_id");
             String name =rs.getString("class_name");
             //System.out.println(id);
             Clases cl = new Clases(name,id);
             clas.add(cl);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return clas;
}
public Clases editclas(Clases clas){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update clases set class_id='"+clas.getClass_id()+"', class_name='"+clas.getClass_name()+"' where class_id='"+clas.getClass_id()+"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return clas; 
} 
public Clases deleteClas(Clases clas){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from clases where class_id ='"+clas.getClass_id()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return clas;  
}
public boolean addstream(Streams newstream){
    Connection con =this.db.getConnection();
    boolean ans=false;
    try{
        String insert="insert into streams(stream_id,stream_name,class_id)values('"+newstream.getStream_id()+"','"+newstream.getStream_name()+"','"+newstream.getClass_id()+"')" ;
        Statement st = con.createStatement();
        System.out.println(insert);
        st.execute(insert);
        ans=true;
        st.close();
         con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return ans;
}
public ArrayList allStreams(){
    ArrayList<Streams> stream = new ArrayList<Streams>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select * from streams";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             String strmid=rs.getString("stream_id");
             String name =rs.getString("stream_name");
             int id =rs.getInt("class_id");
             Streams str = new Streams(strmid,name,id);
             stream.add(str);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return stream;
}
public Streams edistream(Streams strm){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update streams set stream_id='"+strm.getStream_id()+"', stream_name='"+strm.getStream_name()+"', class_id='"+strm.getClass_id()+"' where stream_id='"+strm.getStream_id()+"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return strm;
} 
public Streams deleteStrem(Streams str){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from streams where streams ='"+str.getStream_id()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return str;  
}
public Streams searchStream(Streams str){
 
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select stream_id,stream_name,class_id from streams where stream_name like '%"+str.getStream_name()+"%'";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
            String strmid=rs.getString("stream_id");
            String name =rs.getString("stream_name");
            int id =rs.getInt("class_id");
            Streams strm = new Streams(strmid,name,id);
            
         }
        }catch(Exception e){
            e.printStackTrace();
        }
        return str;
}
 public boolean registerStudent(Students newstudent){
    Connection con =this.db.getConnection();
    boolean ans=false;
    try{
        String add = "insert into student(name,class,dob,guardian,contacts)values('"+newstudent.getName()+
        "','"+newstudent.getGrade()+"','"+newstudent.getDob()+"','"+newstudent.getGuardian()+"','"+newstudent.getContacts()+"')";         
        System.out.println(add);
        Statement st =con.createStatement();
        st.execute(add);
        ans=true;
        st.close();
         con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return ans;
}
public ArrayList allStudent(){
    ArrayList<Students> stud = new ArrayList<Students>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select admno,name,class,dob,guardian,contacts from student";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             String admn=rs.getString("admno");
             String name =rs.getString("name");
             String clas =rs.getString("class");
             String dob =rs.getString("dob");
             String parent =rs.getString("guardian");
             int contact = rs.getInt("contacts");
             Students std = new Students(admn,name,clas,dob,parent,contact );
             stud.add(std);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return stud;
}
public Students editstud(Students std){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update student set admno='"+std.getAdmno()+"',name='"+std.getName()+"', class='"+std.getGrade()+"' where admno='"+std.getAdmno()+"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return std;
} 
public Students deleteStud(Students std){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from student where admno ='"+std.getAdmno()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return std;  
}
public boolean addfee(Fees newfee, int clas){
    Feesdetail detail = new Feesdetail();
    Connection con =this.db.getConnection();
    boolean ans=false;
    try{
        String add = "insert into fees(item,amount,clas)values('"+newfee.getItem()+"','"+newfee.getAmount()+
        "','"+newfee.getClas()+"')";         
        System.out.println(add);
        Statement st =con.createStatement();
        st.execute(add);

        //getting total fee
        int total_fee =0;
        String sum="select sum(amount)from fees where clas='"+clas+"'"; 
        System.out.println(sum);
        ResultSet rss= st.executeQuery(sum);
        while(rss.next()){
           int total =rss.getInt(1);
            total_fee = total_fee+total; 
           detail.setTotalfee(total_fee);
           int tot= detail.getTotalfee();
           System.out.println("The total fee is$>"+tot);  
       }
        ans=true;
        st.close();
         con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return ans;
}
public ArrayList allFees( int claz){
    ArrayList<Feesdetail> fees = new ArrayList<Feesdetail>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select item,amount from fees where clas ="+claz;
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             String item=rs.getString("item");
             int amount = rs.getInt("amount");;
            /// int clas = rs.getInt("clas");
             
            Feesdetail fee = new Feesdetail(item,amount );
             fees.add(fee);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return fees;
}

}
