/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author essy
 */
public class Clases {
    private int class_id;
    private String class_name;
    private int school_id;
    
    public int getClass_id() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }
    public int getSchool_id() {
        return school_id;
    }

    public void setSchool_id(int school_id) {
        this.school_id = school_id;
    }

    public Clases(int class_id, String class_name,int school_id) {
        this.class_id = class_id;
        this.class_name = class_name;
        this.school_id = school_id;
    }
    public Clases( String class_name,int class_id) {
        this.class_name = class_name;
        this.class_id = class_id;
    }
    public Clases(){
        
    }
    
    
}
