/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author essy
 */
public class Fees {
    private String item;
    private int amount;
    private int clas;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getClas() {
        return clas;
    }

    public void setClas(int clas) {
        this.clas = clas;
    }

    public Fees(String item, int amount, int clas) {
        this.item = item;
        this.amount = amount;
        this.clas = clas;
    }
    public Fees( int clas) {;
        this.clas = clas;
    }
    public Fees() {
    }
    
 
}
