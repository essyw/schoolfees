/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author essy
 */
public class Feesdetail {
    private String item;
    private int amount;
    private int clas;
    private int totalfee;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getClas() {
        return clas;
    }

    public void setClas(int clas) {
        this.clas = clas;
    }
    public int getTotalfee() {
        return totalfee;
    }

    public void setTotalfee(int totalfee) {
        this.totalfee = totalfee;
    }

    public Feesdetail(String item, int amount, int clas) {
        this.item = item;
        this.amount = amount;
        this.clas = clas;
    }
    public Feesdetail(String item, int amount) {
        this.item = item;
        this.amount = amount;
    }
    public Feesdetail() {
    }
    public Feesdetail(int clas) {
        this.clas = clas;
    }
 
}
