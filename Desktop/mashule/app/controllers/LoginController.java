package controllers;

import play.mvc.*;

import views.html.*;
import play.db.*;
import javax.inject.*;
import play.data.*;
import services.SchoolOperations;
import services.Users;
import services.Login;
import services.Students;
import services.Routes;
import services.Roles;
import java.util.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class LoginController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    private SchoolOperations schoolop;
    play.data.FormFactory fmm;
   @ Inject
public LoginController(SchoolOperations schoolop, play.data.FormFactory fmm){
  this.schoolop=schoolop;
  this.fmm=fmm;
}
public Result login() {
    System.out.println("method>>>>>"+request().method());
    if(request().method().equals("GET")){
        return ok(login.render());
    }else{
       DynamicForm requestData=fmm.form().bindFromRequest();
       Login log = new Login(requestData.get("email"),requestData.get("password"));
       schoolop.getLogin(log);
       String name=log.getName();
       session("logeduser",name);
       session("logedrole",log.getRole());
       String logd=session().get("logeduser");
       String roll=session().get("logedrole");
    System.out.println("the logged user is...."+logd);
    System.out.println("the logged userrole is...."+roll);  
          return ok(homepage.render());

    }       
}
public Result logout(){
    session().clear();
    return ok(logout.render());
}

}
