package controllers;

import play.mvc.*;

import views.html.*;
import play.db.*;
import javax.inject.*;
import play.data.*;
import services.SchoolOperations;
import services.Users;
import services.Login;
import services.Students;
import services.Routes;
import services.Roles;
import services.Clases;
import services.Streams;
import services.Picklocations;
import forms.Vehicles;
import forms.Drivers;
import forms.Matrons;
import forms.Vehicleroute;
import forms.Vehicledriver;
import forms.Vehiclematron;
import forms.Studentdetails;
import services.AdminOnlySecurity;
import java.util.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */

public class AdminController extends Controller {
   private SchoolOperations schoolop;
    play.data.FormFactory fmm;
   @ Inject

public AdminController(SchoolOperations schoolop, play.data.FormFactory fmm){
  this.schoolop=schoolop;
  this.fmm=fmm;
}
    public Result index() {
        return ok(index.render("Your new application is ready."));
    }
    public Result homepage() {
        return ok(homepage.render());
    }
    public Result registeruser() {
        Users us = new Users();
        ArrayList<Roles> role=  schoolop.getRole(us);
        int no = role.size();
       System.out.println("the no of roles are....>"+no);
    if(request().method().equals("GET")){
        return ok(registeruser.render(role));   
    }else{
        DynamicForm requestData =fmm.form().bindFromRequest();
        Users newUser = new Users(requestData.get("name"),requestData.get("email"),
        requestData.get("password"),requestData.get("role"));
       boolean reg=schoolop.registerUser(newUser);
       if(reg==true){
        flash("message","User Registered");
            return  redirect("/registeruser");
       }else{
       flash("message","user Registration failed");
       }
    }
        return ok(registeruser.render(role));
    }
    public Result addrole(){
        Users us = new Users();
        ArrayList<Roles> role=  schoolop.getRole(us);
        if(request().method().equals("GET")){
            return ok(addrole.render(role));
        }else{
        Form<Roles> roleform= fmm.form(Roles.class);
        Roles newrole= roleform.bindFromRequest().get();
       boolean ans= schoolop.addRole(newrole);
        if(ans==true){
            flash("message","role added");
                return  redirect("/addrole");
           }else{
           flash("message","role addition failed");
           }
        }
        return ok(addrole.render(role)); 
        
    }
    public Result addclass(){
        ArrayList<Clases> clas= schoolop.allclases();
        if(request().method().equals("GET")){
            return ok(addclass.render(clas));
        }else{
            DynamicForm requestData =fmm.form().bindFromRequest();
           String scul= requestData.get("school_id");
           int scl=0;
           try{
            scl=Integer.parseInt(scul);
        }catch(NumberFormatException e){
        }
            Clases newclas = new Clases(requestData.get("class_name"),scl);
        //Form<Clases> clasform=fmm.form(Clases.class);
        //Clases newclas=clasform.bindFromRequest().get();
        boolean ans=schoolop.addclass(newclas);
        if(ans==true){
            flash("message","class added");
                return  redirect("/addclass");
           }else{
           flash("message","class addition failed");
           }
        }
        return ok(addclass.render(clas));
    }
    @Security.Authenticated(AdminOnlySecurity.class)
    public Result editclass(String class_id,String class_name) {
        int id=Integer.parseInt(class_id);
        Clases clas = new Clases(class_name,id);
        ArrayList<Clases> cls= schoolop.allclases();
        if(request().method().equals("GET")){
            return ok(editclass.render(cls,clas));
        }else{
            Form<Clases> classform= fmm.form(Clases.class);
            clas= classform.bindFromRequest().get();
           schoolop.editclas(clas);
        }
        return redirect("/addclass");
    }
    @Security.Authenticated(AdminOnlySecurity.class)
    public Result deleteclass(String class_id,String class_name) {
        int id=Integer.parseInt(class_id);
        Clases clas = new Clases(class_name,id);
        ArrayList<Clases> cls= schoolop.allclases();
        if(request().method().equals("GET")){
            return ok(deleteclass.render(cls,clas));
        }else{
            Form<Clases> classform= fmm.form(Clases.class);
            clas= classform.bindFromRequest().get();
           schoolop.deleteClas(clas);
        }
        return redirect("/addclass");
    }
    public Result addstream(){
        ArrayList<Streams> stream =schoolop.allStreams();
        if(request().method().equals("GET")){
            return ok(addstream.render(stream ));
        }else{
        Form<Streams> streamform=fmm.form(Streams.class);
        Streams newstream=streamform.bindFromRequest().get();
        boolean ans=schoolop.addstream(newstream);
        if(ans==true){
            flash("message","stream added");
                return  redirect("/addstream");
           }else{
           flash("message","stream addition failed");
           }
        }
        return ok(addstream.render(stream ));
    }
    public Result allstreams(){
        Streams strm =  new Streams();
        Streams streamz=schoolop.searchStream(strm);
        ArrayList<Streams> stream =schoolop.allStreams();
        if(request().method().equals("GET")){
            return ok(allstreams.render(stream,streamz ));
        }else{
        Form<Streams> streamform=fmm.form(Streams.class);
        Streams newstream=streamform.bindFromRequest().get();
        boolean ans=schoolop.addstream(newstream);
   
        return ok(allstreams.render(stream,streamz ));
    }
}
    @Security.Authenticated(AdminOnlySecurity.class)
    public Result editstream(String stream_id,String stream_name,String class_id) {
        int id=Integer.parseInt(class_id);
        Streams strm = new Streams(stream_id,stream_name,id);
        ArrayList<Streams> stream= schoolop.allStreams();
        if(request().method().equals("GET")){
            return ok(editstream.render(strm));
        }else{
            Form<Streams> streamform= fmm.form(Streams.class);
            strm= streamform.bindFromRequest().get();
           schoolop.edistream(strm);
        }
        return redirect("/addstream");
    }
    @Security.Authenticated(AdminOnlySecurity.class)
    public Result deletestream(String stream_id,String stream_name,String class_id) {
        int id=Integer.parseInt(class_id);
        Streams strm = new Streams(stream_id,stream_name,id);
        ArrayList<Streams> stream= schoolop.allStreams();
        if(request().method().equals("GET")){
            return ok(deletestream.render(strm));
        }else{
            Form<Streams> streamform= fmm.form(Streams.class);
            strm= streamform.bindFromRequest().get();
           schoolop.deleteStrem(strm);
        }
        return redirect("/addstream");
    
    }
    public Result registerstudent() {
        ArrayList<Streams> stream=schoolop.allStreams();
        ArrayList<Students> stud=schoolop.allStudent();
        if(request().method().equals("GET")){
            return ok(registerstudent.render(stud,stream));   
        }else{;
            DynamicForm requestData =fmm.form().bindFromRequest();
         String contact=requestData.get("contacts");
         int cont=0;
         try{
         cont=Integer.parseInt(contact);
        }catch(NumberFormatException e){
        }   
           Students newstudent = new Students(requestData.get("name"),requestData.get("admno"),
         requestData.get("grade"),requestData.get("dob"),requestData.get("guardian"),cont);
            //Form<Students> studentForm = fmm.form(Students.class);
            //Students newstudent = studentForm.bindFromRequest().get();
            schoolop.registerStudent(newstudent);
        return ok(registerstudent.render(stud,stream));
    }
}
public Result allstudents() {
    ArrayList<Students> stud=schoolop.allStudent();
    
        return ok(allstudents.render(stud)); 
    }
@Security.Authenticated(AdminOnlySecurity.class)
public Result editstudent(String admno,String name,String grade) {
    Students std = new Students(admno,name,grade);
    ArrayList<Streams> stream=schoolop.allStreams();
    ArrayList<Students> stud=schoolop.allStudent();
    if(request().method().equals("GET")){
        return ok(editstudent.render(stud,stream,std));
    }else{
        DynamicForm requestData =fmm.form().bindFromRequest();
            Students newstudent = new Students(requestData.get("admno"),requestData.get("name"),
            requestData.get("grade"));
        //Form<Students> studentForm = fmm.form(Students.class);
        //Students newstudent = studentForm.bindFromRequest().get();
      schoolop.editstud(newstudent);
    }
    return redirect("/registerstudent");
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result deletestudent(String admno,String name,String grade) {
    Students std = new Students(admno,name,grade);
    ArrayList<Students> stud=schoolop.allStudent();
    ArrayList<Streams> stream=schoolop.allStreams();
    if(request().method().equals("GET")){
        return ok(deletestudent.render(stud,stream,std));
    }else{
        Form<Students> studentform= fmm.form(Students.class);
        std= studentform.bindFromRequest().get();
       schoolop.deleteStud(std);
    }
    return redirect("/registerstudent");

}
public  Result managestudent() {
    ArrayList<Students> stud=schoolop.allStudent();
        return ok(managestudent.render(stud)); 
    }
public  Result studentroute(String admno) {
    ArrayList<Routes> rout=schoolop.allRoutes();
    Students std = new Students(admno);
    System.out.println(admno);
    if(request().method().equals("GET")){
          return ok(studentroute.render(std,rout)); 
        }else{
            DynamicForm requestData =fmm.form().bindFromRequest();
            Students newstudent = new Students(requestData.get("admno"),requestData.get("route"));
            schoolop.routestud(newstudent); 
    return ok(studentroute.render(std,rout));            
        } 
    }
 public  Result studentpickpoint(String admno) {
    ArrayList<Picklocations> pick=schoolop.allpickdroppoints();
        Students std = new Students(admno);
        System.out.println(admno);
        if(request().method().equals("GET")){
              return ok(studentpickpoint.render(std,pick)); 
            }else{
                DynamicForm requestData =fmm.form().bindFromRequest();
                Students newstudent = new Students(requestData.get("admno"),requestData.get("route"));
                schoolop.pickpointstud(newstudent); 
        return ok(studentpickpoint.render(std,pick));            
            } 
        }
public Result addroute() {
    ArrayList<Routes> rout=schoolop.allRoutes(); 
    if(request().method().equals("GET")){ 
        return ok(addroute.render(rout)); 
    }else{
        Form<Routes> routform= fmm.form( Routes.class);
        Routes newroute =routform.bindFromRequest().get();
       boolean ans= schoolop.addRoute(newroute);
       if(ans==true){
        flash("message","route added");
        return  redirect("/addroute");
       }else{
        flash("message","route addition failed"); 
       }
    return ok(addroute.render(rout));
}
}
public Result allroutes() {
    ArrayList<Routes> rout=schoolop.allRoutes(); 
    
        return ok(allroutes.render(rout));
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result editroute(String route_id,String route_name) {
    int id=Integer.parseInt(route_id);
    Routes rout = new Routes(id,route_name);
    ArrayList<Routes> rt= schoolop.allRoutes();
    if(request().method().equals("GET")){
        return ok(editroute.render(rt,rout));
    }else{
        Form<Routes> routform= fmm.form(Routes.class);
        rout= routform.bindFromRequest().get();
       schoolop.editrout(rout);
    }
    return redirect("/addroute");
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result deleteroute(String route_id,String route_name) {
    int id=Integer.parseInt(route_id);
    Routes rout = new Routes(id,route_name);
    ArrayList<Routes> rt= schoolop.allRoutes();
    if(request().method().equals("GET")){
        return ok(deleteroute.render(rt,rout));
    }else{
        Form<Routes> routorm= fmm.form(Routes.class);
        rout=  routorm.bindFromRequest().get();
       schoolop.deleterout(rout);
    }
    return redirect("/addroute");
}
public Result assignroute(String vehicle_id,String vehicle_name) {
    Vehicleroute vehicles = new Vehicleroute(vehicle_id,vehicle_name);
   ArrayList<Routes> route = schoolop.allRoutes();
   if(request().method().equals("GET")){
        return ok(assignroute.render(vehicles,route));
    }else{
        DynamicForm requestData =fmm.form().bindFromRequest();
       
        Vehicleroute vrout = new Vehicleroute(requestData.get("vehicle_id"),requestData.get("name"),
        requestData.get("route_name"));
     // Form<Vehicleroute> vecroutform= fmm.form(Vehicleroute.class);
      //  Vehicleroute vrout= vecroutform.bindFromRequest().get();
       schoolop.assingroute(vrout);  
    } 
    return ok(assignroute.render(vehicles,route));   
}
public Result addpicklocation() {
    ArrayList<Routes> route =schoolop.allRoutes();
    ArrayList<Picklocations> pick=schoolop.allpickdroppoints();
    if(request().method().equals("GET")){
        return ok(addpicklocation .render(pick,route));   
    }else{
       // DynamicForm requestData =fmm.form().bindFromRequest();
       // String rot=requestData.get("routeid");
       // int rout=Integer.parseInt(rot);
       // Picklocations newpick = new Picklocations(requestData.get("locationid"),requestData.get("pickdrop"),
        //rout);
        Form<Picklocations> pickform= fmm.form(Picklocations.class);
       Picklocations newpick= pickform.bindFromRequest().get();
       boolean ans= schoolop.addPickUplocations(newpick);
       if(ans==true){
        flash("message","location added");
        return  redirect("/addpicklocation");
       }else{
        flash("message","location addition failed"); 
        return  redirect("/addpicklocation");
       }
    }
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result editpicklocation(String locationid,String pickdrop,String routeid) {
 //  int id=Integer.parseInt(routeid);
  //  Picklocations pikdrop = new Picklocations(locationid,pickdrop,id);
  //  ArrayList<Routes> route =schoolop.allRoutes();
   // ArrayList<Picklocations> picdrop= schoolop.allpickdroppoints();
   // if(request().method().equals("GET")){
   //     return ok(editpicklocation.render(picdrop,route,pikdrop));
   // }else{
     //   DynamicForm requestData =fmm.form().bindFromRequest();
      //  String rot=requestData.get("routeid");
       // int rout=Integer.parseInt(rot);
       // pikdrop = new Picklocations(requestData.get("locationid"),requestData.get("pickdrop"),
       // rout);
        //Form<Picklocations> locationform= fmm.form(Picklocations.class);
        //pikdrop= locationform.bindFromRequest().get();
     //  schoolop.editpickdroppoints(pikdrop);
   // }
    return redirect("/addpicklocation");
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result deletepicklocation(String locationid,String pickdrop,String routeid) {
    //int id=Integer.parseInt(routeid);
    //Picklocations pikdrop = new Picklocations(locationid,pickdrop,id);
    //ArrayList<Routes> route =schoolop.allRoutes();
    //ArrayList<Picklocations> picdrop= schoolop.allpickdroppoints();
    //if(request().method().equals("GET")){
    //    return ok(deletepicklocation.render(picdrop,route,pikdrop));
    //}else{
    //        Form<Picklocations> locationform= fmm.form(Picklocations.class);
    //        pikdrop= locationform.bindFromRequest().get();
    //      schoolop.deletepickdroppoints(pikdrop);
    //}
    return redirect("/addpicklocation");

}
public Result addvehicles() {
    Vehicles vec = new Vehicles();
    Vehicles vehicle = schoolop.searchvehicle(vec);
    ArrayList<Vehicles> vehicles =schoolop.allvehicles();
    if(request().method().equals("GET")){
        return ok(addvehicles .render(vehicles));   
    }else{
        Form<Vehicles> vehicleform= fmm.form(Vehicles.class);
        Vehicles newvehicle =vehicleform.bindFromRequest().get();
       boolean ans= schoolop.addVehicle(newvehicle);
       if(ans==true){
        flash("message","vehicle added");
        return  redirect("/addvehicles");
       }else{
        flash("message","vehicle addition failed"); 
        return  redirect("/addvehicles");
       }
    }   
}
public Result allvehicles() {
    ArrayList<Vehicles> vehicles =schoolop.allvehicles();
    
        return ok(allvehicles .render(vehicles));
}
public Result managevehicles() {
    ArrayList<Vehicles> vehicles =schoolop.allvehicles();
    ArrayList<Matrons> mat = schoolop.allmatrons();
    ArrayList<Drivers> driver = schoolop.alldrivers();
    ArrayList<Routes> rout=schoolop.allRoutes(); 
        return ok(managevehicles .render(vehicles,rout,driver,mat));
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result editvehicle(String vehicle_id,String name) {
    
    Vehicles vehicle = new Vehicles(vehicle_id,name);
    ArrayList<Vehicles> vec= schoolop.allvehicles();
    if(request().method().equals("GET")){
        return ok(editvehicle.render(vec,vehicle));
    }else{
        Form<Vehicles> vehicleform= fmm.form(Vehicles.class);
        vehicle= vehicleform.bindFromRequest().get();
       schoolop.editvehicles(vehicle);
    }
    return redirect("/addvehicles");
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result deletevehicle(String vehicle_id,String name) {
    
    Vehicles vehicle = new Vehicles(vehicle_id,name);
    ArrayList<Vehicles> vec= schoolop.allvehicles();
    if(request().method().equals("GET")){
        return ok(deletevehicle.render(vec,vehicle));
    }else{
        Form<Vehicles> vehicleform= fmm.form(Vehicles.class);
        vehicle= vehicleform.bindFromRequest().get();
       schoolop.deletevehicles(vehicle);
    }
    return redirect("/addvehicles");

}
public Result addrivers() {
    ArrayList<Drivers> driver = schoolop.alldrivers();
    if(request().method().equals("GET")){
        return ok(addrivers.render(driver));   
    }else{
        Form<Drivers> driverform= fmm.form(Drivers.class);
        Drivers newdriver =driverform.bindFromRequest().get();
       boolean ans= schoolop.addDrivers(newdriver);
       if(ans==true){
        flash("message","driver added");
        return  redirect("/addrivers");
       }else{
        flash("message","driver addition failed"); 
        return  redirect("/addrivers");
       }
    }
    
}
public Result alldrivers() {
    ArrayList<Drivers> driver = schoolop.alldrivers();
    
        return ok(alldrivers .render(driver));
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result editdriver(String driver_id,String driver_name,String vehicle_id) {
    Drivers dr = new Drivers(driver_id,driver_name,vehicle_id);
    ArrayList<Drivers> drv= schoolop.alldrivers();
    if(request().method().equals("GET")){
        return ok(editdriver.render(drv,dr));
    }else{
        Form<Drivers> driverform= fmm.form(Drivers.class);
        dr= driverform.bindFromRequest().get();
       schoolop.editdrivers(dr);
    }
    return redirect("/addrivers");
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result deletedriver(String driver_id,String driver_name,String vehicle_id) {
    Drivers dr = new Drivers(driver_id,driver_name,vehicle_id);
    ArrayList<Drivers> drv= schoolop.alldrivers();
    if(request().method().equals("GET")){
        return ok(deletedriver.render(drv,dr));
    }else{
        Form<Drivers> driverform= fmm.form(Drivers.class);
        dr= driverform.bindFromRequest().get();
       schoolop.deletedrivers(dr);
    }
    return redirect("/addrivers");
}
public Result assigndriver(String vehicle_id,String vehicle_name) {
    Vehicledriver vehicles = new Vehicledriver(vehicle_id,vehicle_name);
    ArrayList<Drivers> driver = schoolop.alldrivers();
   if(request().method().equals("GET")){
        return ok(assigndriver.render(vehicles,driver));
    }else{
        Form<Vehicledriver> vecdriverform= fmm.form(Vehicledriver.class);
        Vehicledriver vdriver= vecdriverform.bindFromRequest().get();
       schoolop.assingdriver(vdriver);  
    } 
    return ok(assigndriver.render(vehicles,driver));   
}
public Result addmatron(){
    ArrayList<Matrons> mat = schoolop.allmatrons();
    if(request().method().equals("GET")){
        return ok(addmatron.render(mat));   
    }else{
        Form<Matrons> matronform= fmm.form(Matrons.class);
        Matrons newmatron =matronform.bindFromRequest().get();
       boolean ans= schoolop.addMatrons(newmatron);
       if(ans==true){
        flash("message","matron added");
        return  redirect("/addmatron");
       }else{
        flash("message","matron addition failed"); 
        return  redirect("/addmatron");
       }
    }
}
public Result allmatrons() {
    ArrayList<Matrons> mat = schoolop.allmatrons();
    
        return ok(allmatrons  .render(mat));
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result editmatron(String matrons_id,String matrons_name,String vehicle_id) {
    Matrons matron = new Matrons(matrons_id,matrons_name,vehicle_id);
    ArrayList<Matrons> mat = schoolop.allmatrons();;
    if(request().method().equals("GET")){
        return ok(editmatron.render(mat,matron));
    }else{
        Form<Matrons> matronform= fmm.form(Matrons.class);
        matron= matronform.bindFromRequest().get();
       schoolop.editmatrons(matron);
    }
    return redirect("/addmatron");
}
@Security.Authenticated(AdminOnlySecurity.class)
public Result deletematron(String matrons_id,String matrons_name,String vehicle_id) {
    Matrons matron = new Matrons(matrons_id,matrons_name,vehicle_id);
    ArrayList<Matrons> mat = schoolop.allmatrons();;
    if(request().method().equals("GET")){
        return ok(deletematron.render(mat,matron));
    }else{
        Form<Matrons> matronform= fmm.form(Matrons.class);
        matron= matronform.bindFromRequest().get();
       schoolop.deletematrons(matron);
    }
    return redirect("/addmatron");
}
public Result assignmatron(String vehicle_id,String vehicle_name) {
    Vehiclematron matron = new Vehiclematron(vehicle_id,vehicle_name);
    ArrayList<Matrons> mat = schoolop.allmatrons();
   if(request().method().equals("GET")){
        return ok(assignmatron.render(matron,mat));
    }else{
        Form<Vehiclematron> matronform= fmm.form(Vehiclematron.class);
        Vehiclematron vmatron= matronform.bindFromRequest().get();
       schoolop.assingmatron(vmatron);  
    } 
    return ok(assignmatron.render(matron,mat));   
}
}
