/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

/**
 *
 * @author essy
 */
public class Vehicledriver {
    private String vehicle_id;
    private String name;
    private String driver_name;

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }
   

    public Vehicledriver(String vehicle_id, String name,String driver_name) {
        this.vehicle_id = vehicle_id;
        this.name = name;
        this.driver_name = driver_name;
    }
    public Vehicledriver(String vehicle_id, String name) {
        this.vehicle_id = vehicle_id;
        this.name = name;
    }

    public Vehicledriver() {
    }
    
    
}
