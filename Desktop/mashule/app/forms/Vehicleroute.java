/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

/**
 *
 * @author essy
 */
public class Vehicleroute {
    private String vehicle_id;
    private String name;
    private String route_name;

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getRoute_name() {
        return route_name;
    }

    public void setRoute_id(String route_name) {
        this.route_name = route_name;
    }
   

    public Vehicleroute(String vehicle_id, String name,String route_name) {
        this.vehicle_id = vehicle_id;
        this.name = name;
        this.route_name = route_name;
    }
    public Vehicleroute(String vehicle_id, String name) {
        this.vehicle_id = vehicle_id;
        this.name = name;
    }

    public Vehicleroute() {
    }
    
    
}
