/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

/**
 *
 * @author essy
 */
public class Matrons {
   private String matrons_id;
   private String matrons_name;
   private String vehicle_id;

    public String getMatrons_id() {
        return matrons_id;
    }

    public void setMatrons_id(String matrons_id) {
        this.matrons_id = matrons_id;
    }

    public String getMatrons_name() {
        return matrons_name;
    }

    public void setMatrons_name(String matrons_name) {
        this.matrons_name = matrons_name;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public Matrons(String matrons_id, String matrons_name, String vehicle_id) {
        this.matrons_id = matrons_id;
        this.matrons_name = matrons_name;
        this.vehicle_id = vehicle_id;
    }

    public Matrons() {
    }
   
}
