/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

/**
 *
 * @author essy
 */
public class Schools {
    private int school_id;
    private String school_name;

    public int getSchool_id() {
        return school_id;
    }

    public void setSchool_id(int school_id) {
        this.school_id = school_id;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public Schools(int school_id, String school_name) {
        this.school_id = school_id;
        this.school_name = school_name;
    }
    public Schools(){
        
    } 
    public Schools(String school_name){
        this.school_name = school_name; 
    } 
}
