/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

/**
 *
 * @author essy
 */
public class Vehicles {
    private String vehicle_id;
    private String name;
  

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   

    public Vehicles(String vehicle_id, String name) {
        this.vehicle_id = vehicle_id;
        this.name = name;
        
    }

    public Vehicles() {
    }
    
    
}
