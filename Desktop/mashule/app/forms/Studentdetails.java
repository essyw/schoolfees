
    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

/**
 *
 * @author essy
 */
public class Studentdetails {
    private String name;
    private String admno;
    private String grade;
    private int contacts;
    private String route;
    private String pickPoint;
    private String  droPoints;
    private String  bus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdmno() {
        return admno;
    }

    public void setAdmno(String admno) {
        this.admno = admno;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public int getContacts() {
        return contacts;
    }

    public void setContacts(int contacts) {
        this.contacts = contacts;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getPickPoint() {
        return pickPoint;
    }

    public void setPickPoint(String pickPoint) {
        this.pickPoint = pickPoint;
    }

    public String getDroPoints() {
        return droPoints;
    }

    public void setDroPoints(String droPoints) {
        this.droPoints = droPoints;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public Studentdetails(String name, String admno, String grade, int contacts, String route, String pickPoint, String droPoints, String bus) {
        this.name = name;
        this.admno = admno;
        this.grade = grade;
        this.contacts = contacts;
        this.route = route;
        this.pickPoint = pickPoint;
        this.droPoints = droPoints;
        this.bus = bus;
    }

    public Studentdetails(String admno){
        this.admno = admno;
    }
    public Studentdetails(){
    }
}
