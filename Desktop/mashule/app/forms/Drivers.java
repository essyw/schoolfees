/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

/**
 *
 * @author essy
 */
public class Drivers {
    private String driver_id;
    private String driver_name;
    private String vehicle_id;

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public Drivers(String driver_id, String driver_name, String vehicle_id) {
        this.driver_id = driver_id;
        this.driver_name = driver_name;
        this.vehicle_id = vehicle_id;
    }

    public Drivers() {
    }
    
    
}
