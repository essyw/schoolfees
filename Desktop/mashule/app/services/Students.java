/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author essy
 */
public class Students {

    private String name;
    private String admno;
    private String grade;
    private String dob;
    private String guardian;
    private int contacts;
    private String route;
    private String pickPoint;
    private String  droPoints;
    private String  bus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdmno() {
        return admno;
    }

    public void setAdmno(String admno) {
        this.admno = admno;
    }

    public String getGrade(){
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGuardian() {
        return guardian;
    }

    public void setGuardian(String guardian) {
        this.guardian = guardian;
    }

    public int getContacts() {
        return contacts;
    }

    public void setContacts(int contacts) {
        this.contacts = contacts;
    }
    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }
    public String getPickPoint() {
        return pickPoint;
    }
    public void setPickPoint(String pickPoint) {
        this.pickPoint = pickPoint;
    }
    public Students(String admno,String name,String grade) {
        this.name = name;
        this.admno = admno;
        this.grade = grade;
       
    }
    public Students(String name, String admno, String grade, String dob, String guardian, int contacts) {
        this.name = name;
        this.admno = admno;
        this.grade = grade;
        this.dob = dob;
        this.guardian = guardian;
        this.contacts = contacts;
    }
    public Students() {
    }
    public Students(String admno,String route){
        this.admno = admno;
        this.route = route;
    }
    public Students(String admno){
        this.admno = admno;
    }
    
}
