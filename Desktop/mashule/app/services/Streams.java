/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author essy
 */
public class Streams {
    private String stream_id;
    private String stream_name;
    private int class_id;

    public String getStream_id() {
        return stream_id;
    }

    public void setStream_id(String stream_id) {
        this.stream_id = stream_id;
    }

    public String getStream_name() {
        return stream_name;
    }

    public void setStream_name(String stream_name) {
        this.stream_name = stream_name;
    }

    public int getClass_id() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id = class_id;
    }

    public Streams(String stream_id, String stream_name, int class_id) {
        this.stream_id = stream_id;
        this.stream_name = stream_name;
        this.class_id = class_id;
    }

    public Streams() {
    }
    
}
