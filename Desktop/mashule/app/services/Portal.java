/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import javax.inject.Singleton;
import javax.inject.*;
import play.db.*;
import java.util.*; 
import java.text.SimpleDateFormat;
import forms.Schools;
import forms.Allschools;
/**
 *
 * @author essy
 */
@Singleton
public class Portal{
 Database db;
 @Inject
 public Portal(Database db){
     this.db=db; 
 }
 public boolean createSchool(Schools newscul){
    boolean ans=false;
     Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String insert="insert into schools(school_name)values('"+newscul.getSchool_name()+"')";
         st.execute(insert);
         ans=true;
     }catch(Exception e){
         e.printStackTrace();
     }
     return ans;
}
public ArrayList allSchools(){
    ArrayList<Schools> school = new ArrayList<Schools>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select school_id,school_name from schools";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             int id =rs.getInt("school_id");
             String name =rs.getString("school_name");
             Schools scul = new Schools(id,name);
             school.add(scul);
         }
        }catch(Exception e){
            e.printStackTrace();
        }
        return school;
}
public ArrayList searchSchools(Allschools allschool){
    ArrayList<Allschools> school = new ArrayList<Allschools>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select school_id,school_name from schools where school_name like '%"+allschool.getSchool_name()+"%'";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             String id =rs.getString("school_id");
             String name =rs.getString("school_name");
             Allschools scul = new Allschools(id,name);
             school.add(scul);
         }
        }catch(Exception e){
            e.printStackTrace();
        }
        return school;
}
public Allschools allSchoolcount(Allschools school){
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
        String count="select count(school_id) from schools";
        System.out.println(count);
        ResultSet rss= st.executeQuery(count);
        while(rss.next()){
      int idd=rss.getInt(1);
      school.setAllschool(idd);
      System.out.println("totalschool count>>>>>>"+idd);
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        return school;
}
public Schools editSchool(Schools scul){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update schools set school_id='"+scul.getSchool_id()+"', school_name='"+scul.getSchool_name()+"' where school_id='"+scul.getSchool_id()+"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return scul;  
}
public Schools deleteSchool(Schools scul){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from schools where school_id ='"+scul.getSchool_id()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return scul;  
}
}
