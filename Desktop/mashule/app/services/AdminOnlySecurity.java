
package services;

import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
//import views.model.LoginTemplate;

public class AdminOnlySecurity extends Security.Authenticator {
    @Override
    public String getUsername(Http.Context ctx) {

     //check username and  roleid

        if (ctx.session().containsKey("username")&&(ctx.session().get("role").equals("admin")||ctx.session().get("role").equals("manager"))) {
            return ctx.session().get("username");
        }else {
            return null;
        }
    }

    @Override
    public Result onUnauthorized(Http.Context context) {
        if (!context.session().containsKey("username")){
            context.flash().put("message", "Login to access system");
            return redirect("/login");
        }else {

            context.flash().put("message", "Not Authorized to access page");

            return redirect("/homepage");
        }

    }

    private String getTokenFromHeader(Http.Context ctx) {
        String[] authTokenHeaderValues = ctx.request().headers().get("X-AUTH-TOKEN");
        if ((authTokenHeaderValues != null) && (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)) {
            return authTokenHeaderValues[0];
        }else {
            return null;
        }
    }
}