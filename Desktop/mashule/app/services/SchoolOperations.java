/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import javax.inject.Singleton;
import javax.inject.*;
import play.db.*;
import java.util.*; 
import java.text.SimpleDateFormat;
import forms.Vehicles;
import forms.Drivers;
import forms.Matrons;
import forms.Vehicleroute;
import forms.Vehicledriver;
import forms.Vehiclematron;
import forms.Studentdetails;
/**
 *
 * @author essy
 */
@Singleton
public class SchoolOperations {
 Database db;
 @Inject
 public SchoolOperations(Database db){
     this.db=db; 
 }
public boolean addRole(Roles newrole){
    Connection con =this.db.getConnection();
boolean ans=false;
    try{
        Statement st = con.createStatement();
        String add="insert into roles (role_id,role_name) values('"+newrole.getRole_id()+"','"+newrole.getRole_name()+"')";
        st.execute(add);
        ans=true;
    st.close();
    con.close();
    }catch (Exception e){
        e.printStackTrace();
    }
    return ans;
}
 public ArrayList getRole(Users newuser){
    boolean registerd=false;
    ArrayList<Roles> role = new ArrayList<Roles>();
    Connection con =this.db.getConnection();
    try{
       Statement st = con.createStatement();
       String select = "Select role_name from roles";
       ResultSet rs = st.executeQuery(select);
       while(rs.next()){
           String rolename=rs.getString("role_name");
           Roles rl = new Roles(rolename);
           role.add(rl);
       }
        st.close();
        con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return role;
}
 public boolean registerUser(Users newuser){
     boolean registerd=false;
     ArrayList<Roles> role = new ArrayList<Roles>();
     Connection con =this.db.getConnection();
     try{
        Statement st = con.createStatement();
         String insert="insert into users(name,username,password,role)values('"+newuser.getName()+
                 "','"+newuser.getUserName()+"','"+newuser.getPassword()+"','"+newuser.getRole()+"')" ;
         st.execute(insert);
         System.out.println(insert);
         registerd=true;
         st.close();
         con.close();
     }catch(Exception e){
         e.printStackTrace();
     }
     return registerd;
 }
 public Login getLogin(Login newlogin){
    Connection con =this.db.getConnection();
    Login log=null;
    String pattern = "yyyy-MM-dd HH:mm:ss";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    try{
        String select="select name,username,password,role from users where username ='"
        +newlogin.getEmail()+"' && password='"+newlogin.getPassword()+"'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(select);
        while(rs.next()){
            String named=rs.getString("name");
            newlogin.setName(named);
            String emaild=rs.getString("username");
            String password=rs.getString("password");
            String roled=rs.getString("role");
            newlogin.setRole(roled);
            log = new Login(named,emaild,password,roled);
        }
        String time="update users set logintime='"+ simpleDateFormat.format(new Date())+"' where username='"+newlogin.getEmail()+"' && password='"+newlogin.getPassword()+"'";
        Statement stt = con.createStatement();
        stt.execute(time);
        st.close();
         con.close();
    }catch(Exception e){
      e.printStackTrace();  
    }
    return log; 
 }
 public boolean addclass(Clases newclas){
    Connection con =this.db.getConnection();
    boolean ans=false;
    try{
        String insert="insert into clases(class_name,school_id)values('"+newclas.getClass_name()+"','"+newclas.getSchool_id()+"')" ;
        Statement st = con.createStatement();
        st.execute(insert);
        ans=true;
        st.close();
         con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return ans;
}
public ArrayList allclases(){
    ArrayList<Clases> clas = new ArrayList<Clases>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select * from clases";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             int id =rs.getInt("class_id");
             String name =rs.getString("class_name");
             int scul =rs.getInt("school_id");
             Clases cl = new Clases(id,name,scul);
             clas.add(cl);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return clas;
}
public Clases editclas(Clases clas){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update clases set class_id='"+clas.getClass_id()+"', class_name='"+clas.getClass_name()+"' where class_id='"+clas.getClass_id()+"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return clas; 
} 
public Clases deleteClas(Clases clas){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from clases where class_id ='"+clas.getClass_id()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return clas;  
}
public boolean addstream(Streams newstream){
    Connection con =this.db.getConnection();
    boolean ans=false;
    try{
        String insert="insert into streams(stream_id,stream_name,class_id)values('"+newstream.getStream_id()+"','"+newstream.getStream_name()+"','"+newstream.getClass_id()+"')" ;
        Statement st = con.createStatement();
        System.out.println(insert);
        st.execute(insert);
        ans=true;
        st.close();
         con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return ans;
}
public ArrayList allStreams(){
    ArrayList<Streams> stream = new ArrayList<Streams>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select * from streams";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             String strmid=rs.getString("stream_id");
             String name =rs.getString("stream_name");
             int id =rs.getInt("class_id");
             Streams str = new Streams(strmid,name,id);
             stream.add(str);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return stream;
}
public Streams edistream(Streams strm){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update streams set stream_id='"+strm.getStream_id()+"', stream_name='"+strm.getStream_name()+"', class_id='"+strm.getClass_id()+"' where stream_id='"+strm.getStream_id()+"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return strm;
} 
public Streams deleteStrem(Streams str){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from streams where streams ='"+str.getStream_id()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return str;  
}
public Streams searchStream(Streams str){
 
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select stream_id,stream_name,class_id from streams where stream_name like '%"+str.getStream_name()+"%'";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
            String strmid=rs.getString("stream_id");
            String name =rs.getString("stream_name");
            int id =rs.getInt("class_id");
            Streams strm = new Streams(strmid,name,id);
            
         }
        }catch(Exception e){
            e.printStackTrace();
        }
        return str;
}
 public boolean registerStudent(Students newstudent){
    Connection con =this.db.getConnection();
    boolean ans=false;
    try{
        String add = "insert into student(admno,name,class,dateof_birth,guardian,contacts)values('"+newstudent.getAdmno()+"','"+newstudent.getName()+
        "','"+newstudent.getGrade()+"','"+newstudent.getDob()+"','"+newstudent.getGuardian()+"','"+newstudent.getContacts()+"')";         
        System.out.println(add);
        Statement st =con.createStatement();
        st.execute(add);
        ans=true;
        st.close();
         con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
    return ans;
}
public ArrayList allStudent(){
    ArrayList<Students> stud = new ArrayList<Students>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select admno,name,class,dateof_birth,guardian,contacts from student";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             String admn=rs.getString("admno");
             String name =rs.getString("name");
             String clas =rs.getString("class");
             String dob =rs.getString("dateof_birth");
             String parent =rs.getString("guardian");
             int contact = rs.getInt("contacts");
             Students std = new Students(admn,name,clas,dob,parent,contact );
             stud.add(std);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return stud;
}
public Students editstud(Students std){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update student set admno='"+std.getAdmno()+"',name='"+std.getName()+"', class='"+std.getGrade()+"' where admno='"+std.getAdmno()+"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return std;
} 
public Students deleteStud(Students std){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from student where admno ='"+std.getAdmno()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return std;  
}
public Students routestud(Students std){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String update ="update student set route='"+std.getRoute()+"' where admno='"+std.getAdmno()+"'";
        System.out.println(update);
        st.execute(update);
    }catch(Exception e){
        e.printStackTrace();
    }
    return std;
} 
public Students pickpointstud(Students std){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String update ="update student set pickpoint='"+std.getPickPoint()+"' where admno='"+std.getAdmno()+"'";
        System.out.println(update);
        st.execute(update);
    }catch(Exception e){
        e.printStackTrace();
    }
    return std;
} 
public Boolean addRoute(Routes newroute){
    Connection con = this.db.getConnection();
    boolean ans=false;
    try{
        String add="insert into routes(route_id,route_name)values('"+newroute.getRoute_id()+"','"+
        newroute.getRoute_name()+"')";
        System.out.println(add);
        Statement st=con.createStatement();
        st.execute(add);
        ans=true;
        st.close();
         con.close();
    }catch(Exception e){
        e.printStackTrace();
    }
  return ans;  
}
public ArrayList allRoutes(){
    ArrayList<Routes> route = new ArrayList<Routes>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select route_id,route_name from routes";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             int id =rs.getInt("route_id");
             String name =rs.getString("route_name");
             Routes rt = new Routes(id,name);
             route.add(rt);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return route;
}
public Routes editrout(Routes rt){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update routes set route_id='"+rt.getRoute_id()+"', route_name='"+
        rt.getRoute_name()+"' where route_id='"+rt.getRoute_id()+"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return rt; 
} 
public Routes deleterout(Routes rt){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from routes where route_name ='"+rt.getRoute_name()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return rt;  
}
public Boolean addPickUplocations(Picklocations newpicklocation){
    Connection con =this.db.getConnection();
    boolean ans=false;
    try{
        String newlocation="insert into pickup_points(pickupid, pickdroppoint_name,routname)values('"+newpicklocation.getLocationid()+
        "','"+newpicklocation.getPickdrop()+ "','"+newpicklocation.getRoutname()+"')";
        Statement st =con.createStatement();
        System.out.println(newlocation);
        st.execute(newlocation);
        ans=true;
        st.close();
        con.close(); 
        
    }catch(Exception e){
          e.printStackTrace();
      }
      return ans;  
  }
  public ArrayList allpickdroppoints(){
    ArrayList<Picklocations> pdpoints = new ArrayList<Picklocations>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select pickupid,pickdroppoint_name,routeid from pickup_points";
         System.out.println(select);
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             String pickid=rs.getString("pickupid");
             String name =rs.getString("pickdroppoint_name");
             String rout =rs.getString("routeid");
             Picklocations pd = new Picklocations(pickid,name,rout);
             pdpoints.add(pd);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return pdpoints;
}
public Picklocations editpickdroppoints(Picklocations pickdrop){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update pickup_points set pickupid='"+pickdrop.getLocationid()+"',pickdroppoint_name='"+pickdrop.getPickdrop()+"', routename='"+pickdrop.getRoutname()
        +"' where pickupid='"+pickdrop.getLocationid()+"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return pickdrop;
} 
public Picklocations  deletepickdroppoints(Picklocations pickdrop ){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from pickup_points where pickupid='"+pickdrop.getLocationid()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return pickdrop;  
}
public Boolean addVehicle(Vehicles newvehicle){
    Connection con =this.db.getConnection();
    boolean ans=false;
    try{
        String insert="insert into vehicles(vehicle_id, vehicle_no)values('"+newvehicle.getVehicle_id()+"','"+newvehicle.getName()+"')";
        Statement st =con.createStatement();
        System.out.println(insert);
        st.execute(insert);
        ans=true;
        st.close();
        con.close(); 
        
    }catch(Exception e){
          e.printStackTrace();
      }
      return ans;  
  }
  public ArrayList allvehicles(){
    ArrayList<Vehicles> vehicles = new ArrayList<Vehicles>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select vehicle_id,vehicle_no from vehicles";
         System.out.println(select);
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             String vehicleid=rs.getString("vehicle_id");
             String name =rs.getString("vehicle_no");
            
             Vehicles vcl = new Vehicles(vehicleid,name);
             vehicles.add(vcl);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return vehicles;
}
public Vehicles editvehicles(Vehicles vec){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update vehicles set vehicle_id='"+vec.getVehicle_id()+"', vehicle_no='"+
        vec.getName()+" where vehicle_id='"+vec.getVehicle_id()+"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return vec; 
}
public Vehicles deletevehicles(Vehicles vec ){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from vehicles where vehicle_id='"+vec.getVehicle_id()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return vec;  
} 
public Vehicles searchvehicle(Vehicles vec){
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select vehicle_id,vehicle_no from vehicles where vehicle_no like '%"+vec.getName()+"%'";
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
            String vehicleid=rs.getString("vehicle_id");
            String name =rs.getString("vehicle_no");
          
            Vehicles vcl = new Vehicles(vehicleid,name);
         }
        }catch(Exception e){
            e.printStackTrace();
        }
        return vec;
}
public Vehicleroute assingroute(Vehicleroute newroute){
    Connection con =this.db.getConnection();
   
    try{
        String update="update vehicles set route_name ='"+newroute.getRoute_name()+"' where vehicle_id='"+newroute.getVehicle_id()+"'";
        Statement st =con.createStatement();
        System.out.println(update);
        st.execute(update);
        st.close();
        con.close(); 
        
    }catch(Exception e){
          e.printStackTrace();
      }
      return newroute;  
  }
public Boolean addDrivers(Drivers newdriver){
    Connection con =this.db.getConnection();
    boolean ans=false;
    try{
        String insert="insert into drivers(driver_id, driver_name)values('"+newdriver.getDriver_id() +
 "','"+newdriver.getDriver_name()+"')";
        Statement st =con.createStatement();
        System.out.println(insert);
        st.execute(insert);
        ans=true;
        st.close();
        con.close(); 
        
    }catch(Exception e){
          e.printStackTrace();
      }
      return ans;  
  }
  public ArrayList alldrivers(){
    ArrayList<Drivers> driver = new ArrayList<Drivers>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select driver_id,driver_name,vehicle from drivers";
         System.out.println(select);
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             String driverid=rs.getString("driver_id");
             String name =rs.getString("driver_name");
             String vehicle=rs.getString("vehicle");
             Drivers drv = new Drivers(driverid,name,vehicle);
             driver.add(drv);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return driver;
}
public Drivers editdrivers(Drivers dr){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update drivers set driver_id='"+dr.getDriver_id()+"', driver_name='"+
        dr.getDriver_name()+"',vehicle='"+dr.getVehicle_id()+"' where driver_id='"+dr.getDriver_id()+"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return dr; 
}
public Drivers deletedrivers(Drivers dr){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from drivers where driver_id='"+dr.getDriver_id()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return dr;  
} 
public Vehicledriver assingdriver(Vehicledriver newdriver){
    Connection con =this.db.getConnection();
   
    try{
        Statement st =con.createStatement();
        String update="update vehicles set driver_name ='"+newdriver.getDriver_name()+"' where vehicle_id='"+newdriver.getVehicle_id()+"'";
        System.out.println(update);
        st.execute(update);
        st.close();
        con.close(); 
        
    }catch(Exception e){
          e.printStackTrace();
      }
      return newdriver;  
  }
public Boolean addMatrons(Matrons newmatron){
    Connection con =this.db.getConnection();
    boolean ans=false;
    try{
        String insert="insert into matrons(matron_id, matron_name)values('"+newmatron.getMatrons_id() +
 "','"+newmatron.getMatrons_name()+"')";
        Statement st =con.createStatement();
        System.out.println(insert);
        st.execute(insert);
        ans=true;
        st.close();
        con.close(); 
        
    }catch(Exception e){
          e.printStackTrace();
      }
      return ans;  
  }
  public ArrayList allmatrons(){
    ArrayList<Matrons> matron = new ArrayList<Matrons>();
    Connection con =this.db.getConnection();
     try{
         Statement st =con.createStatement();
         String select ="select matron_id,matron_name,vehicle from matrons";
         System.out.println(select);
         ResultSet rs = st.executeQuery(select);
         while(rs.next()){
             String matronid=rs.getString("matron_id");
             String name =rs.getString("matron_name");
             String vehicle=rs.getString("vehicle");
             Matrons mat= new Matrons(matronid,name,vehicle);
             matron.add(mat);
         }
         st.close();
             con.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return matron;
}
public Matrons editmatrons(Matrons mat){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String edit ="update matrons set matron_id='"+mat.getMatrons_id()+"', matron_name='"+
        mat.getMatrons_name()+"',vehicle='"+mat.getVehicle_id()+"' where matron_id='"+mat.getMatrons_id() +"'";
        System.out.println(edit);
        st.execute(edit);
    }catch(Exception e){
        e.printStackTrace();
    }
    return mat; 
}
public Matrons deletematrons(Matrons mat){
    Connection con =this.db.getConnection();
    try{
        Statement st =con.createStatement();
        String delete ="delete from matrons where matron_id='"+mat.getMatrons_id()+"'";
        System.out.println(delete);
        st.execute(delete);
    }catch(Exception e){
        e.printStackTrace();
    }
    return mat;  
} 
public Vehiclematron assingmatron(Vehiclematron newmatron){
    Connection con =this.db.getConnection();
   
    try{
        Statement st =con.createStatement();
        String update="update vehicles set matron_name ='"+newmatron.getMatron_name()+"' where vehicle_id='"+newmatron.getVehicle_id()+"'";
        System.out.println(update);
        st.execute(update);
        st.close();
        con.close(); 
        
    }catch(Exception e){
          e.printStackTrace();
      }
      return newmatron;  
  }
}
